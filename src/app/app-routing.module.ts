import { NgModule } from '@angular/core';
import { Routes, RouterModule, RouteReuseStrategy } from '@angular/router';
import { RouteReusableStrategy } from './router-reusable.strategy';
import { AuthGuards } from './store/auth.guards';
import { NotFoundComponent } from './not-found/not-found.component';


const routes: Routes = [
  {
      path: "login",
      loadChildren: () => import(`./login/login.module`).then(x => x.LoginModule)
  },
  {
      path: "tasks",
      canLoad: [AuthGuards],
      loadChildren: () => import(`./tasks/tasks.module`).then(x => x.TasksModule)
  },
  {
      path: "",
      redirectTo: "login",
      pathMatch: "full"
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
      { provide: RouteReuseStrategy, useClass: RouteReusableStrategy },
      AuthGuards,
  ],
})
export class AppRoutingModule { }
