import { Component, ChangeDetectionStrategy } from '@angular/core';
import { IUser } from './interfaces/interfaces';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { IAuthState } from './store/auth.reducers';
import { getUserData } from './store/auth.selectors';
import { logout } from './store/auth.actions';

@Component({
  selector: 'u-root',
  template: `
    <u-loader></u-loader>
    <mat-toolbar *ngIf="user$ | async">
      <mat-toolbar-row>
        <span class="logo">ucode test</span>
        <button mat-button routerLink="/tasks">/tasks</button>
        <button mat-button routerLink="/login">/login</button>
        <span class="example-spacer"></span>
        <button mat-button (click)="logout()">Logout ({{ (user$ | async).name }} [{{ (user$ | async).role }}])</button>
      </mat-toolbar-row>
    </mat-toolbar>
    <router-outlet></router-outlet>
  `,
  styles: [`
  .example-spacer {
    flex: 1 1 auto;
  }
  .logo {
    margin-right: 20px;
    user-select: none;
  }`],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
    user$: Observable<IUser>;

    constructor(private authStore: Store<IAuthState>) {}
    ngOnInit() {
      this.user$ = this.authStore.select(getUserData);
    }
    logout() {
      this.authStore.dispatch(logout());
    }
}
