import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: string): string {
    const firstLetter = value.split('').shift();
    if (firstLetter === 'i') {
      return 'In Progress';
    } else if (firstLetter === 't') {
      return 'To Do';
    } else if (firstLetter === 'd') {
      return 'Done';
    } else {
      return '';
    }
  }

}
