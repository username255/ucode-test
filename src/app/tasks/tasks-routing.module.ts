import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksComponent } from './tasks/tasks.component';
import { TasksResolver } from './tasks.resolver';
import { CanActivateTasks } from './tasks-activate.guard';


const routes: Routes = [
  {
    path: '',
    component: TasksComponent,
    canActivate: [CanActivateTasks],
    resolve: { data: TasksResolver },
  },
  {
    path: ':id',
    component: TasksComponent,
    resolve: { data: TasksResolver },
    data: { reuse: true }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [TasksResolver, CanActivateTasks]
})
export class TasksRoutingModule { }
