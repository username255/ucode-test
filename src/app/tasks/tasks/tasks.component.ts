import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Observable } from 'rxjs';
import { getTasks, getTasksToDo, getTasksInProgress, getTasksDone, getDevelopers } from '../store/data.selectors';
import { Store } from '@ngrx/store';
import { map, filter } from 'rxjs/operators';
import { ITask, IDeveloper } from 'src/app/interfaces/interfaces';
import { IDataState } from '../store/data.reducers';
import { statusUpdate, switchAssignee } from '../store/data.actions';
import { ActivatedRoute } from '@angular/router';
import { IAuthState } from 'src/app/store/auth.reducers';
import { getUserRole } from 'src/app/store/auth.selectors';

@Component({
    selector: "u-tasks",
    templateUrl: "./tasks.component.html",
    styleUrls: ["./tasks.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksComponent implements OnInit {

    // tasks$: Observable<ITask[]>;

    toDo$: Observable<ITask[]>;
    inProgress$: Observable<ITask[]>;
    done$: Observable<ITask[]>;

    developers$: Observable<IDeveloper[]>;

    filterByDeveloperId: number = undefined;

    isManager$: Observable<boolean>;

    constructor(private store: Store<IDataState>, private userStore: Store<IAuthState>, private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.params.subscribe((params) => {
          if (params && params.id) {
            this.filterByDeveloperId = +(params.id);
            if (isNaN(this.filterByDeveloperId)) {
              this.filterByDeveloperId = undefined;
            }
          }
        });

        this.toDo$ = this.store.select(getTasksToDo).pipe(
          map((tasks) => ((!this.filterByDeveloperId) ? tasks : (tasks).filter((task) => (task.developerId === this.filterByDeveloperId))))
        );
        this.inProgress$ = this.store.select(getTasksInProgress).pipe(
          map((tasks) => ((!this.filterByDeveloperId) ? tasks : (tasks).filter((task) => (task.developerId === this.filterByDeveloperId))))
        );
        this.done$ = this.store.select(getTasksDone).pipe(
          map((tasks) => ((!this.filterByDeveloperId) ? tasks : (tasks).filter((task) => (task.developerId === this.filterByDeveloperId))))
        );
        this.developers$ = this.store.select(getDevelopers).pipe(
          map((developers) => ((!this.filterByDeveloperId) ? developers : (developers).filter((dev) => (dev.id === this.filterByDeveloperId))))
        );
        this.isManager$ = this.store.select(getUserRole).pipe(
          map((role) => (role === 'manager'))
        );
    }

    statusChange(e) {
      this.store.dispatch(statusUpdate(e));
    }

    assigneeChange(e) {
      this.store.dispatch(switchAssignee(e));
    }

}
