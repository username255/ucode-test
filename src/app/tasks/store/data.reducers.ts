import { IDeveloper, ITask } from 'src/app/interfaces/interfaces';
import { createReducer, on } from '@ngrx/store';
import { loadDevelopersSuccess, loadTasksSuccess, statusUpdateSuccess, switchAssigneeSuccess, loadDevelopers, loadTasks, statusUpdate, switchAssignee, loadDevelopersFail, loadTasksFail, statusUpdateFail, switchAssigneeFail } from './data.actions';

export const dataReducersFractal = 'data';

export interface IDataState {
    developers: IDeveloper[];
    tasks: ITask[];
    loading: boolean;
}

export const initialState: IDataState = {
    developers: [],
    tasks: [],
    loading: false
}

export const dataReducers = createReducer(
    initialState,
    on(loadDevelopersSuccess, (state, { developers }) => ({
        ...state,
        developers,
        loading: false
    })),
    on(loadTasksSuccess, (state, { tasks, developers }) => ({
        ...state,
        tasks,
        developers,
        loading: false
    })),
    on(statusUpdateSuccess, (state, { status, id }) => {
        const tasks = state.tasks.map((task) => (
            (task.id === id) ? ({ ...task, status }) : (task)
        ));
        return {
            ...state,
            tasks,
            loading: false
        }
    }),
    on(switchAssigneeSuccess, (state, { id, developerId }) => {
        const dev = state.developers.find((dev) => (developerId === dev.id));
        if (!dev) {
            throw new Error('Reassigned task to an unknown developer!');
        }
        const tasks = state.tasks.map((task) => (
            (task.id === id) ? ({ ...task, developerId, name: dev.name, avatar: dev.avatar }) : (task)
        ));
        return {
            ...state,
            tasks,
            loading: false
        }
    }),
    on(loadDevelopers, loadTasks, statusUpdate, switchAssignee, (state, {}) => ({
        ...state,
        loading: true
    })),
    on(loadDevelopersFail, loadTasksFail, statusUpdateFail, switchAssigneeFail, (state, {}) => ({
        ...state,
        loading: false
    })),
);