import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, filter, first, map, switchMap, withLatestFrom, tap } from 'rxjs/operators';
import { loadTasks, loadTasksSuccess, loadTasksFail, loadDevelopers, loadDevelopersSuccess, loadDevelopersFail, statusUpdate, switchAssignee, statusUpdateSuccess, statusUpdateFail, switchAssigneeSuccess, switchAssigneeFail } from './data.actions';
import { DataService } from '../../services/data.service';

@Injectable()
export class DataEffects {

    public constructor(
        private actions$: Actions,
        private dataService: DataService,
    ) { }

    @Effect() loadTasks$ = this.actions$.pipe(
        ofType(loadTasks),
        switchMap(({ id }) => this.dataService.getTasks(id).pipe(
            map(({ tasks, developers }) => loadTasksSuccess({ tasks, developers })),
            catchError(err => of(loadTasksFail(err))),
        )),
    );

    @Effect() loadDevelopers$ = this.actions$.pipe(
        ofType(loadDevelopers),
        switchMap(() => this.dataService.getDevelopers().pipe(
            map(developers => loadDevelopersSuccess({ developers })),
            catchError(err => of(loadDevelopersFail(err))),
        )),
    );

    @Effect() statusUpdate$ = this.actions$.pipe(
        ofType(statusUpdate),
        switchMap(({ id, status }) => this.dataService.updateTaskStatus(id, status).pipe(
            map(() => statusUpdateSuccess({ id, status })),
            catchError(err => of(statusUpdateFail(err))),
        )),
    );

    @Effect() switchAssignee$ = this.actions$.pipe(
        ofType(switchAssignee),
        switchMap(({ id, developerId }) => this.dataService.switchTaskAssignee(id, developerId).pipe(
            map(() => switchAssigneeSuccess({ id, developerId })),
            catchError(err => of(switchAssigneeFail(err))),
        )),
    );

}
