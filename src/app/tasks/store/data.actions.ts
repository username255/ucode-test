import { createAction, props } from "@ngrx/store";
import { HttpErrorResponse } from '@angular/common/http';
import { IDeveloper, ITask } from 'src/app/interfaces/interfaces';

export const loadDevelopers = createAction("[Data] Load developers");
export const loadDevelopersSuccess = createAction("[Data] Load developers success",
    props<{ developers: IDeveloper[] }>());
export const loadDevelopersFail = createAction("[Data] Load developers fail",
    props<{ error: HttpErrorResponse }>());

export const loadTasks = createAction("[Data] Load tasks",
    props<{ id: number }>());
export const loadTasksSuccess = createAction("[Data] Load tasks success",
    props<{ tasks: ITask[], developers: IDeveloper[] }>());
export const loadTasksFail = createAction("[Data] Load tasks fail",
    props<{ error: HttpErrorResponse }>());

export const statusUpdate = createAction("[Data] Status update",
    props<{ status: string, id: number }>());
export const statusUpdateSuccess = createAction("[Data] Status update success",
    props<{ status: string, id: number }>());
export const statusUpdateFail = createAction("[Data] Status update fail",
    props<{ error: HttpErrorResponse }>());

export const switchAssignee = createAction("[Data] Switch assignee",
    props<{ developerId: number, id: number }>());
export const switchAssigneeSuccess = createAction("[Data] Switch assignee success",
    props<{ developerId: number, id: number }>());
export const switchAssigneeFail = createAction("[Data] Switch assignee fail",
    props<{ error: HttpErrorResponse }>());
