import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IDataState, dataReducersFractal } from './data.reducers';

export const getDataState = createFeatureSelector<IDataState>(dataReducersFractal);
export const getTasks = createSelector(getDataState, state => state.tasks);
export const getTasksToDo = createSelector(getDataState, state => state.tasks.filter((task) => (task.status === 'toDo')));
export const getTasksInProgress = createSelector(getDataState, state => state.tasks.filter((task) => (task.status === 'inProgress')));
export const getTasksDone = createSelector(getDataState, state => state.tasks.filter((task) => (task.status === 'done')));
export const getDevelopers = createSelector(getDataState, state => state.developers);
export const getLoading = createSelector(getDataState, state => state.loading);