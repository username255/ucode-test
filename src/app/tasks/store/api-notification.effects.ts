import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { loadTasksFail, statusUpdateFail, switchAssigneeFail, switchAssigneeSuccess, statusUpdateSuccess, loadTasks, statusUpdate, switchAssignee, loadTasksSuccess } from './data.actions';

@Injectable()
export class ApiNotificationEffects {

    public constructor(private actions$: Actions, private snackBar: MatSnackBar) { }

    @Effect({ dispatch: false }) loadTaskFail$ = this.actions$.pipe(
        ofType(loadTasksFail),
        tap(({ error }) => {
            const msg = (error && error.message + ' ') || `An error occurred while loading the task!`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) statusUpdateFail$ = this.actions$.pipe(
        ofType(statusUpdateFail),
        tap(({ error }) => {
            const msg = (error && error.message + ' ') || `An error occurred while updating the task status!`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) switchAssigneeFail$ = this.actions$.pipe(
        ofType(switchAssigneeFail),
        tap(({ error }) => {
            const msg = (error && error.message + ' ') || `An error occurred while updating the task assignee!`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) switchAssigneeSuccess$ = this.actions$.pipe(
        ofType(switchAssigneeSuccess),
        tap(() => {
            const msg = `Updated!`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) statusUpdateSuccess$ = this.actions$.pipe(
        ofType(statusUpdateSuccess),
        tap(() => {
            const msg = `Updated!`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) loadTasks$ = this.actions$.pipe(
        ofType(loadTasks),
        tap(() => {
            const msg = `Loading...`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) loadTasksSuccess$ = this.actions$.pipe(
        ofType(loadTasksSuccess),
        tap(() => {
            const msg = `Loaded!`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) updateStarted$ = this.actions$.pipe(
        ofType(statusUpdate, switchAssignee),
        tap(() => {
            const msg = `Updating...`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    private openSnackBar(message: string, action: string, duration: number = 3000) {
        this.snackBar.open(message, action, {
            duration: duration,
            panelClass: 'multiline'
        });
    }
}
