import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, zip, of, race } from 'rxjs';
import { Store, State } from '@ngrx/store';
import { map, take, first, concat } from 'rxjs/operators';
import { Actions, ofType } from '@ngrx/effects';
import { IDataState } from './store/data.reducers'
import { loadTasks, loadTasksFail, loadTasksSuccess } from './store/data.actions'
import { getUserId, getUserToken, getUserData } from '../store/auth.selectors';


@Injectable()
export class TasksResolver implements Resolve<any> {

    constructor(
        private store: Store<IDataState>,
        private authStore: Store<IDataState>,
        private actions$: Actions,
        private router: Router
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        this.authStore.select(getUserData).subscribe((user) => {
            const routeId = route.params['id'];
            if (!user) {
                this.router.navigate(['/login']);
            }
            if (!isNaN(+routeId)) {
                this.store.dispatch(loadTasks({ id: routeId }));
            } else if (!isNaN(+user.id)) {
                this.store.dispatch(loadTasks({ id: user.id }))
            } else {
                return of(false);
            }
        });
        const loadTasksSuccess$ = this.actions$.pipe(ofType(loadTasksSuccess));
        const loadTasksFail$ = this.actions$.pipe(ofType(loadTasksFail));
        return loadTasksSuccess$.pipe(concat(loadTasksFail$), first());

    }

}
