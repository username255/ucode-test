import { Component, OnInit, ChangeDetectionStrategy, Input, ViewChild, Output, EventEmitter } from "@angular/core";
import { ITask, IDeveloper } from 'src/app/interfaces/interfaces';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: "u-task",
    templateUrl: "./task.component.html",
    styleUrls: ["./task.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskComponent implements OnInit {

    @Input() task: ITask;
    @Input() isManager: boolean;
    @Input() developers: IDeveloper[];
    @Output() assignee = new EventEmitter<{ developerId: string, id: number }>();
    @Output() status = new EventEmitter<{ status: string, id: number }>();

    @ViewChild('select', {static: false}) select;
    @ViewChild('assignee', {static: false}) assigneeSelect;

    edit = false;

    statusForm: FormGroup;
    assigneeForm: FormGroup;
    private _id: number;

    constructor(private fb: FormBuilder) {}

    ngOnInit() {
      this.statusForm = this.fb.group({
        status: new FormControl(this.task.status),
      });
      this.assigneeForm = this.fb.group({
        assignee: new FormControl(this.task.developerId),
      });
    }

    ngAfterViewInit() {
      this._id = this.task.id;
      this.statusForm.valueChanges.subscribe(({ status }) => {
        this.status.emit({ status, id: this._id });
      });
      this.assigneeForm.valueChanges.subscribe(({ assignee }) => {
        this.assignee.emit({ developerId: assignee, id: this._id });
      });
    }

    openEditor() {
      this.select.open();
    }

    openAssigneeSelect() {
      this.assigneeSelect.open();
    }

}
