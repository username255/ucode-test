import { NgModule, InjectionToken } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksRoutingModule } from './tasks-routing.module';
import { TasksComponent } from './tasks/tasks.component';

import { StoreModule, ActionReducerMap } from '@ngrx/store';
import { IDataState, dataReducersFractal, dataReducers } from './store/data.reducers';
import { DataService } from '../services/data.service';
import { EffectsModule } from '@ngrx/effects';
import { DataEffects } from './store/data.effects';
import { TaskComponent } from './task/task.component';
import { MaterialModule } from '../material.module';
import { StatusPipe } from '../status.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiNotificationEffects } from './store/api-notification.effects';

export const DATA_REDUCER_TOKEN = new InjectionToken<
  ActionReducerMap<IDataState>
>('Data Reducers');

@NgModule({
  declarations: [
    TasksComponent,
    TaskComponent,
    StatusPipe,
  ],
  imports: [
    CommonModule,
    TasksRoutingModule,
    StoreModule.forFeature(dataReducersFractal, DATA_REDUCER_TOKEN),
    EffectsModule.forFeature([DataEffects, ApiNotificationEffects]),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: DATA_REDUCER_TOKEN,
      useValue: dataReducers,
    },
    DataService,
  ],
})
export class TasksModule { }
