import { CanLoad, CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { isAuthenticatedAsManager, getUserData } from '../store/auth.selectors';
import { IAuthState } from '../store/auth.reducers';

@Injectable()
export class CanActivateTasks implements CanActivate {

    constructor(private store: Store<IAuthState>, private router: Router) {}

    public canActivate(): Observable<any> {
        return this.store.select(getUserData).pipe(
            first(),
            map((data) => {
                if (!data) {
                    return this.router.navigate(['/login']);
                }
                if ('manager' === data.role) {
                    return true;
                } else if ('developer' === data.role) {
                    return this.router.navigate(['/tasks', data.id]);
                }
            }));
    }

}
