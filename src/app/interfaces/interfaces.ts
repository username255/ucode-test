export interface IDeveloper {
    id: number;
    name: string;
    username: string;
    avatar: string;
    role: string;
    managerId: number;
}

export interface ITask {
    id: number;
    developerId: number;
    title: string;
    status: string;
    avatar: string;
    name: string;
}

export interface IUser {
    id: number;
    name: string;
    username: string;
    avatar: string;
    role: string;
    token: string;
}
