import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { filter, map, tap, withLatestFrom } from 'rxjs/operators';
import { loginSuccess, logout, checkAuthSuccess, checkAuthFail, loginFail } from './auth.actions';

@Injectable()
export class RouterEffects {

    @Effect({ dispatch: false }) routeToTasks$ = this.actions$.pipe(
        ofType(loginSuccess),
        tap(({ user }) => {
            const role = user.role;
            const id = user.id;
            if (role === 'manager') {
                this.router.navigate(['/tasks']);
            } else if (role === 'developer') {
                this.router.navigate(['/tasks', id]);
            } else {
                this.router.navigate(['/login']);
            }
        }),
    );

    @Effect({ dispatch: false }) navigateToLogin$ = this.actions$.pipe(
        ofType(logout, checkAuthFail, loginFail),
        tap(() => {
            this.router.navigate(['/login']);
        }),
    );

    @Effect({ dispatch: false }) checkAuth$ = this.actions$.pipe(
        ofType(checkAuthSuccess),
        tap(({ user }) => {
            const role = user.role;
            const id = user.id;
            if (role === 'manager') {
                this.router.navigate(['/tasks']);
            } else if (role === 'developer') {
                this.router.navigate(['/tasks', id]);
            } else {
                this.router.navigate(['/login']);
            }
        }),
    );

    constructor(
        private actions$: Actions,
        private router: Router,
    ) { }
}
