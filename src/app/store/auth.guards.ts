import { CanLoad, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAuthState } from './auth.reducers';
import { isAuthenticated } from './auth.selectors';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable()
export class AuthGuards implements CanLoad {

    constructor(private store: Store<IAuthState>) {}

    public canLoad(): Observable<boolean> {
        return this.store.select(isAuthenticated).pipe(first());
    }

}
