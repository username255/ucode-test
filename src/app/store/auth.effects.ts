import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, filter, first, map, switchMap, withLatestFrom, tap } from 'rxjs/operators';
import { login, loginSuccess, loginFail, logout, checkAuth, logoutSuccess, checkAuthSuccess, checkAuthFail } from './auth.actions';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthEffects {

    public constructor(
        private actions$: Actions,
        private authService: AuthService,
    ) { }

    @Effect() login$ = this.actions$.pipe(
        ofType(login),
        switchMap(({ name, pwd }) => this.authService.login(name, pwd).pipe(
            tap(({ token }) => (this.authService.saveTokenToLocalStorage(token))),
            map((user) => loginSuccess({ user })),
            catchError(err => of(loginFail(err))),
        )),
    );

    @Effect() logout$ = this.actions$.pipe(
        ofType(logout),
        switchMap(() => of(this.authService.logout())),
        map(() => logoutSuccess()),
    );

    @Effect() checkAuth$ = this.actions$.pipe(
        ofType(checkAuth),
        switchMap(() => this.authService.checkAuth().pipe(
            map((arr) => (arr[0])),
            map((user) => checkAuthSuccess({ user })),
            catchError(err => of(checkAuthFail(err))),
        )),
        catchError((err) => (of(checkAuthFail(err)))),
    );

}
