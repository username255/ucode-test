import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IAuthState, authReducersFractal } from './auth.reducers';

export const getAuthState = createFeatureSelector<IAuthState>(authReducersFractal);
export const getUserData = createSelector(getAuthState, state => state.user);
export const getUserId = createSelector(getUserData, state => state && state.id);
export const isAuthenticated = createSelector(getAuthState, state => (state.user && true) || false);
export const getUserRole = createSelector(getUserData, state => state && state.role);
export const isAuthenticatedAsManager = createSelector(getUserRole, state => (state === 'manager'));
export const getUserAvatar = createSelector(getUserData, state => state && state.avatar);
export const getUserToken = createSelector(getUserData, state => state && state.token);
export const getLoading = createSelector(getAuthState, state => state.loading);