import { createAction, props } from "@ngrx/store";
import { HttpErrorResponse } from '@angular/common/http';
import { IUser } from 'src/app/interfaces/interfaces';

export const login = createAction("[Auth] Login Attempt",
    props<{ name: string, pwd: string }>());
export const loginSuccess = createAction("[Auth] Login success",
    props<{ user: IUser }>());
export const loginFail = createAction("[Auth] Login fail",
    props<{ error: HttpErrorResponse }>());

export const logout = createAction("[Auth] Logout");
export const logoutSuccess = createAction("[Auth] Logout Success");

export const checkAuth = createAction("[Auth] Check auth");
export const checkAuthSuccess = createAction("[Auth] Check auth success",
    props<{ user: IUser }>());
export const checkAuthFail = createAction("[Auth] Check auth fail",
    props<{ error: HttpErrorResponse }>());
