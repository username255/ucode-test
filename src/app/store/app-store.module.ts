import { InjectionToken, NgModule } from '@angular/core';
import { ActionReducerMap, StoreModule, Store } from '@ngrx/store';
import { NavigationActionTiming, RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { environment } from '../../environments/environment';
import { AppState, appReducers } from './app.reducers';
import { AppEffects } from './app.effects';
import { authReducers, authReducersFractal, IAuthState } from './auth.reducers';
import { checkAuth } from './auth.actions';
import { AuthEffects } from './auth.effects';
import { RouterEffects } from './router.effects';
import { AuthService } from '../services/auth.service';
import { ApiNotificationEffects } from '../tasks/store/api-notification.effects';

export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<AppState>>('Registered Reducers');
export const AUTH_REDUCER_TOKEN = new InjectionToken<ActionReducerMap<AppState>>('Auth Reducers');

@NgModule({
    imports: [
        StoreModule.forRoot(REDUCER_TOKEN),
        StoreModule.forFeature(authReducersFractal, AUTH_REDUCER_TOKEN),
        !environment.production ? StoreDevtoolsModule.instrument() : [],
        EffectsModule.forRoot([AppEffects]),
        EffectsModule.forFeature([
            AuthEffects,
            RouterEffects,
            ApiNotificationEffects
        ]),
    ],
    providers: [
        AuthService,
        { provide: REDUCER_TOKEN, useValue: appReducers },
        { provide: AUTH_REDUCER_TOKEN, useValue: authReducers },
    ],
})
export class AppStoreModule {
    constructor(private store: Store<IAuthState>) {
        this.store.dispatch(checkAuth());
    }
}
