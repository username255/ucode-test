import { ActionReducerMap } from '@ngrx/store';
import { dataReducers, IDataState } from '../tasks/store/data.reducers';
import { IAuthState, authReducers } from './auth.reducers';

export interface AppState {
  data: IDataState;
  user: IAuthState;
}

export const appReducers: ActionReducerMap<AppState> = {
  data: dataReducers,
  user: authReducers
};
