import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { login, loginSuccess, loginFail, logout, checkAuth, logoutSuccess, checkAuthSuccess, checkAuthFail } from './auth.actions';

@Injectable()
export class ApiNotificationEffects {

    public constructor(private actions$: Actions, private snackBar: MatSnackBar) { }

    @Effect({ dispatch: false }) loginFail$ = this.actions$.pipe(
        ofType(loginFail),
        tap(({ error }) => {
            const msg = (error && error.message + '.') || `An error occurred while loging in!`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) loginSuccess$ = this.actions$.pipe(
        ofType(loginSuccess, checkAuthSuccess),
        tap((user) => {
            const name = user && user['name'] || 'user';
            const msg = `Hi ${ name }!`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) loginStarted$ = this.actions$.pipe(
        ofType(login),
        tap(() => {
            const msg = `Checking...`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) logout$ = this.actions$.pipe(
        ofType(logout),
        tap(() => {
            const msg = `Bye-bye!`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    @Effect({ dispatch: false }) checkAuth$ = this.actions$.pipe(
        ofType(checkAuth),
        tap(() => {
            const msg = `Checking your token...`;
            this.openSnackBar(msg, 'Close');
        }),
    );

    private openSnackBar(message: string, action: string, duration: number = 5000) {
        this.snackBar.open(message, action, {
            duration: duration,
            panelClass: 'multiline'
        });
    }
}
