import { IUser } from 'src/app/interfaces/interfaces';
import { createReducer, on } from '@ngrx/store';
import { login, loginSuccess, loginFail, logout, checkAuth, checkAuthSuccess, checkAuthFail } from './auth.actions';

export const authReducersFractal = 'user';

export interface IAuthState {
    user: IUser;
    loading: boolean;
}

export const initialState: IAuthState = {
    user: undefined,
    loading: false
}

export const authReducers = createReducer(
    initialState,
    on(login, (state, {}) => ({
        ...state,
        loading: true
    })),
    on(loginSuccess, (state, { user }) => ({
        ...state,
        user,
        loading: false
    })),
    on(loginFail, (state, {}) => ({
        ...state,
        loading: false
    })),
    on(logout, (state, {}) => ({
        ...state,
        user: undefined,
        loading: false
    })),
    on(checkAuth, (state, {}) => ({
        ...state,
        loading: true
    })),
    on(checkAuthSuccess, (state, { user }) => ({
        ...state,
        user,
        loading: false,
    })),
    on(checkAuthFail, (state, {}) => ({
        ...state,
        loading: false
    })),
);