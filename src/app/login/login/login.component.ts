import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { Store } from '@ngrx/store';
import { IAuthState } from 'src/app/store/auth.reducers';
import { login } from 'src/app/store/auth.actions';

@Component({
    selector: "u-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;

    constructor(private fb: FormBuilder, private store: Store<IAuthState>) {
      this.loginForm = this.fb.group({
        name: new FormControl('', [Validators.required]),
        pwd: new FormControl('', [Validators.required]),
      });
    }

    ngOnInit() {}
    
    onSubmit() {
      this.store.dispatch(login(this.loginForm.value));
    }
}
