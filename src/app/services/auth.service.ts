import { Injectable } from "@angular/core";
import { of, Observable } from "rxjs";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { IUser } from '../interfaces/interfaces';

@Injectable()
export class AuthService {
    baseUrl = environment.serverUrl;

    constructor(private http: HttpClient) {}

    login(name: string, pwd: string): Observable<IUser> {
        const url = this.baseUrl + "/login";
        return this.http.post<IUser>(url, { user: name, pwd });
    }

    logout() {
        this.saveTokenToLocalStorage('');
    }

    checkAuth() {
        const token = this.getTokenFromLocalStorage();
        const url = this.baseUrl + "/users?token=" + token;
        return token && this.http.get<IUser[]>(url);
    }

    getTokenFromLocalStorage() {
        return localStorage.getItem("token");
    }

    saveTokenToLocalStorage(value = "") {
        localStorage.setItem("token", value);
    }
}
