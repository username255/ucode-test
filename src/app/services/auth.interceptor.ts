import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private auth: AuthService) {}

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        const token = this.auth.getTokenFromLocalStorage();
        // if (!token) {
        //     this.auth.logout();
        //     return EMPTY;
        // }
        if (token) {
            const authReq = req.clone({
                headers: req.headers.append("token", token)
            });
            return next.handle(authReq);
        } else {
            return next.handle(req);
        }
    }
}
