import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { IDeveloper, ITask } from '../interfaces/interfaces';

@Injectable()
export class DataService {
    baseUrl = environment.serverUrl;

    constructor(private http: HttpClient) {}

    getDevelopers(): Observable<IDeveloper[]> {
        const url = this.baseUrl + "/developers";
        return this.http.get<IDeveloper[]>(url);
    }

    getTasks(id: number): Observable<{ tasks: ITask[], developers: IDeveloper[] }> {
        const url = this.baseUrl + "/get-tasks?id=" + id;
        return this.http.get<{ tasks: ITask[], developers: IDeveloper[] }>(url);
    }

    switchTaskAssignee(id: number, developerId: number) {
        const url = this.baseUrl + "/task-assignee";
        return this.http.post<any>(url, { id, developerId });
    }

    updateTaskStatus(id: number, status: string) {
        const url = this.baseUrl + "/task-status";
        return this.http.post<any>(url, { id, status });
    }

}

/**
 * 
 
 API:

 {
    "POST: /login { username: string, password: string }": "/login",
    "POST: /task-status { id: number, status: string }": "/task-status",
    "POST: /task-assignee { id: number, assignee: number }": "/task-assignee",
    "POST: /get-tasks": "/get-tasks",
    "GET: /developers": "/developers"
}

 */
