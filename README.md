# Ucode 🚀

## Setup

You'll need node, npm and internet connection to make it work.
`npm i` will install all the necessary dependencies for you.

## Development server

Run `npm run start` and `npm run db` to run app and database in uses.
Navigate to `http://localhost:4200/` to see the app.

## Description

App has basically two routes `/tasks` (and `/tasks/:id`) and `/login`.
Tasks routes are using the same component, though developers could only use `/tasks/:id` with their userId.
`/tasks` route serves as a manager overview of developers available and their current tasks.

All the routes are lazy loaded, though `/tasks` have  `route guards` on them, so you won't be able to load the module without being authenticated as manager or developer.

App also utilizes `ngrx store` capabilities, by having two feature stores named `user` and `data`;
`user` feature store has authenticated user data, while `data` has tasks and developers in it.
`TasksModule` has `/tasks**` routes and also has a lazy loaded ngrx store module (`data`), while `user` is initiated withing `AppModule` (`AppStoreModule` which is imported to `AppModule`).

Error messages are also displayed through store actions and effects with help of `mat-snackbar`.

UI library: I've used `Angular Material` here, hope it's looks fine 🧐

Some responsiveness is also present, so it won't look too bad on small screens.


## Postnotes

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.0.