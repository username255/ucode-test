const DB = require("./db.json");

// flat and flatMap polyfills
if (!Array.prototype.flat) {
	Object.defineProperty(Array.prototype, 'flat', {
		configurable: true,
		value: function flat () {
			var depth = isNaN(arguments[0]) ? 1 : Number(arguments[0]);

			return depth ? Array.prototype.reduce.call(this, function (acc, cur) {
				if (Array.isArray(cur)) {
					acc.push.apply(acc, flat.call(cur, depth - 1));
				} else {
					acc.push(cur);
				}

				return acc;
			}, []) : Array.prototype.slice.call(this);
		},
		writable: true
	});
}
if (!Array.prototype.flatMap) {
	Object.defineProperty(Array.prototype, 'flatMap', {
		configurable: true,
		value: function flatMap (callback) {
			return Array.prototype.map.apply(this, arguments).flat();
		},
		writable: true
	});
}

// login-middleware.js
module.exports = (req, res, next) => {
    // ============== in-memory db (based on provided json) =======
    let db = { ...DB };

    //=========== shared methods ========================
    const getUser = token =>
        db && db.users && db.users.find(user => token === user.token);
    const getUserById = id =>
        db && db.users && db.users.find(user => id === user.id);

    //=============== routes ============================
    if (req.method == "POST" && req.path == "/login") {
        // authentication
        const user_ = req.body && req.body.user;
        const pwd_ = req.body && req.body.pwd;
        const found =
            db.users &&
            db.users.find(user => {
                const username = user["username"];
                const password = user["password"];
                return user_ === username && password === pwd_;
            });
        if (found) {
            // sends user info, all the columns
            res.status(200).json(found);
        } else {
            res.status(401).json({
                message: "wrong username/password combination 😭"
            });
        }
    } else if (req.method == "GET" && req.path == "/get-tasks") {
        // localhost:3000/tasks?developerId=77&developerId=78
        // GET tasks by user id (developer or manager)
        const user_id_ = +(req.query && req.query.id);
        const token_ = req.headers && req.headers.token;
        const userWithProvidedToken = getUser(token_);
        if (!userWithProvidedToken) {
            res.status(401).json({ message: "wrong or missing auth token 🤷‍" });
        } else if (userWithProvidedToken.id !== user_id_ && user_id_ !== userWithProvidedToken.managerId
            && getUserById(user_id_).managerId !== userWithProvidedToken.id) {
            res.status(403).json({ message: "unauthorized 🛑" });
        } else if (userWithProvidedToken.role === "developer") {
            const developers = db.users.filter((user) => (user.id === user_id_)).map(
                ({ token, password, ...rest }) => ({ ...rest })
            );
            const tasks = developers.flatMap((dev) => (
                (db.tasks.filter(task => (task.developerId === dev.id)).map(
                    (t) => ({ ...t, avatar: dev.avatar, name: dev.name })
                ) || [])
            ));
            res.status(200).json({ tasks, developers });
        } else if (userWithProvidedToken.role === "manager") {
            const developers = db.users.filter((user) => (user.managerId === user_id_)).map(
                ({ token, password, ...rest }) => ({ ...rest })
            );
            const tasksByDeveloper = (id) => (db.tasks.filter((task) => (task.developerId === id)));
            const tasks = developers.flatMap((dev) => (
                (tasksByDeveloper(dev.id)).map(
                    (t) => ({ ...t, avatar: dev.avatar, name: dev.name })
                ) || [])
            );
            res.status(200).json({ tasks, developers });
        } else {
            res.status(418).json({ message: "I'm a teapot ☕️" });
        }
    } else if (req.method == "POST" && req.path == "/task-status") {
        // task status update (manager and developer)
        const task_id_ = +(req.body && req.body.id) || 0;
        const token_ = req.headers && req.headers.token;
        const new_status_ = req.body && req.body.status;
        const userWithProvidedToken = getUser(token_);
        if (!userWithProvidedToken) {
            res.status(401).json({ message: "wrong or missing auth token 🤷‍" });
        } else if (
            userWithProvidedToken.role === "manager" ||
            userWithProvidedToken.role === "developer"
        ) {
            db = {
                ...db,
                tasks:
                    db.tasks.map(task => {
                        if (task.id !== task_id_) {
                            return task;
                        } else {
                            return { ...task, status: new_status_ };
                        }
                    }) || []
            };
            res.status(200).json({ message: "status updated! 👍" });
        } else {
            res.status(418).json({ message: "I'm a teapot ☕️" });
        }
    } else if (req.method == "POST" && req.path == "/task-assignee") {
        // TODO: task assignee update (manager only)
        const task_id_ = +(req.body && req.body.id) || 0;
        const token_ = req.headers && req.headers.token;
        const new_assignee_id_ = +(req.body && req.body.assignee) || 0;
        const userWithProvidedToken = getUser(token_);
        if (!userWithProvidedToken) {
            res.status(401).json({ message: "wrong or missing auth token" });
        } else if (userWithProvidedToken.role === "manager") {
            db = {
                ...db,
                tasks:
                    db.tasks.map(task => {
                        if (task.id !== task_id_) {
                            return task;
                        } else {
                            // could be extended to check if this developer id exists...
                            return {
                                ...task,
                                developerId: new_assignee_id_
                            };
                        }
                    }) || []
            };
            res.status(200).json({ message: "reassigned it!" });
        } else if (userWithProvidedToken.role === "developer") {
            res.status(403).json({ message: "unauthorized" });
        } else {
            res.status(418).json({ message: "I'm a teapot ☕️" });
        }
    } else if (req.method == "GET" && req.path == "/developers") {
        // GET developers list for certain manager based on auth token
        const token_ = req.headers && req.headers.token;
        const userWithProvidedToken = getUser(token_);
        if (!userWithProvidedToken) {
            res.status(401).json({ message: "wrong or missing auth token" });
        } else if (userWithProvidedToken.role === "manager") {
            const managerId = userWithProvidedToken.id;
            const developers = (
                db.users.filter(
                    user =>
                        user.role === "developer" &&
                        managerId === user.managerId
                ) || []
            ).map(({ token, password, ...rest }) => rest);
            // shouldn't keep those tokens and passwords in the front-end
            res.status(200).json(developers);
        } else {
            res.status(403).json({ message: "unauthorized" });
        }
    } else if (req.method == "GET" && req.path == "/auth-check") {
        // authentication
        const token = req.headers.token;
        const found = getUser(token);
        if (found) {
            // sends user info, all the columns
            res.status(200).json(found);
        } else {
            res.status(401).json({
                message: "wrong or missing auth token 😿"
            });
        }
    } else {
        next();
    }
};
